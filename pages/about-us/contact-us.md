---
title: Contact Us
description: How to contact the Veilid project
layout: subpage
weight: 2
---

<h3>Contact Us</h3>
<p>For app support, please email <a href="mailto:support@veilid.com">support@veilid.com</a></p>

<h3>Social Media Accounts</h3>
<ul>
  <li><a href="https://twitter.com/cdc_pulpit">cDc on Twitter</a></li>
  <li><a href="https://twitter.com/VeilidNetwork">Veilid on Twitter</a></li>
  <li><a href="https://bsky.app/profile/cultdeadcow.bsky.social">Veilid on BlueSky</a></li>
  <li><a href="https://hackers.town/@veilidnetwork">Veilid on Fedi</a></li>
</ul>
  
