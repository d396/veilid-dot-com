---
title: F.A.Q.
description: Frequently asked questions about Veilid
weight: 6
layout: subpage
---

<ul>
<!--
  <li>
    <h3>Question</h3>
    <p>Answer</p>
  </li>
-->
  <li>
    <h3>What is Veilid?</h3>
    Veilid is an open-source, peer-to-peer, mobile-first, networked application framework. 
  </li>
  <li>
    <h3>Can I install Veilid?</h3>
    You can help the main Veilid network by, <a href="https://gitlab.com/veilid/veilid/-/blob/main/INSTALL.md">running a node</a>.
  </li>
  <li>
    <h3>What about VeilidChat?</h3>
    VeilidChat is under <a href="https://gitlab.com/veilid/veilidchat/-/commits/main">active development</a>. As soon as we have a public test version available, we'll let you know.
  </li>
  <li>
    <h3>If you're so concerned with privacy, why coordinate on Discord?</h3>
    We’re doing what we can with what we have. Since we’re not trying to become a tech circlejerk, we have to meet people on their level. So come on in and help us build something better!
  </li>
  <li>
    <h3>So is all your documentation on Discord?</h3>
    No, official documentation is available in the <a href="https://gitlab.com/veilid">repositories</a>. We also have people working on a <a href="https://veilid.gitlab.io/developer-book/">developer book</a>, a work in progress.
  </li>
  <li>
    <h3>Is Veilid looking for funding?</h3>
    <p>Veilid is not seeking venture capital or investment. We are accepting tax-deductible donations to our non-profit foundation, Veilid Foundation Inc.</p>
  </li>
  <li>
    <h3>Does Veilid have a cryptocurrency?</h3>
    Heck no. Veilid does not have a cryptocurrency. 
  </li>
  <li>
    <h3>Does Veilid use AI?</h3>
    Heck no. Veilid does not use AI. 
  </li>
  <li>
    <h3>Does Veilid use blockchain?</h3>
    Heck no. Veilid does not use blockchain. 
  </li>
</ul>

<div class="clearfix my-2">&nbsp;</div>


<div class="focus-text text-center">
  Have a question? Email <a href="mailto:press@veilid.org" class="text-white">press@veilid.org</a>
</div>

