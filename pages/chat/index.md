---
title: VeilidChat
description: VeilidChat is a proof of concept of the Veilid protocol and framework.
menu:
  main:
    weight: 5
weight: 1
layout: subpage
---

VeilidChat is a demo of the Veilid framework and protocol working.

We built it using the [Flutter](https://flutter.dev/) framework and the Veilid core code.

If you want to try out this proof of concept, please stay tuned for details on how to gain access.

### Source Code

VeilidChat is under active development. The source code is available on [Gitlab](https://gitlab.com/veilid/veilidchat), though there are no official builds yet. 

### Support

<p>For app support, please email <a href="mailto:support@veilid.com">support@veilid.com</a></p>

