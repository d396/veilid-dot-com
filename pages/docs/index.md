---
title: Documentation
description: Documentation for Veilid framework and protocol
menu:
  main:
    weight: 4
weight: 1
layout: subpage
---

The documentation is currently a work in progress.

Are you good at writing? <a href="/about-us/community/">We could use your help</a>.

### Documentation

We're looking to the community to help write out additional documentation, tutorials, and guides.

Ongoing documentation work can be found on [veilid.gitlab.io/developer-book](https://veilid.gitlab.io/developer-book/).

Check out the #documentation channel on our [Discord server](/discord).

### Veilid Nodes

[Install a Veilid Node](https://gitlab.com/veilid/veilid/-/blob/main/INSTALL.md)
