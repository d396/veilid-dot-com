# veilid-dot-com

Important documents to read

- [Contributing](./CONTRIBUTING.md)
- [Code of Conduct](./code_of_conduct.md)
- [Cecil Documentation](https://cecil.app/documentation/)

## Website To-Do

The following are known issues or to-build items as of 2023-08-15

- [x] Move this site to the main Veilid project in Gitlab
- [x] Create an off-season location for DefCon pages
- [ ] Use Cecil's multi-language capabilities for translations
- Documentation and FAQs 
  - [ ] for lay people
  - [ ] for technical people
  - [ ] for cryptography people
  - [ ] for legal entities 

## Thank you!

Thank you to everone helping me keep this updated. 

